import java.util.Scanner;
import models.Carro;

import javax.sound.sampled.SourceDataLine;

public class Main{
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("------Cadastro de Carros------");
        System.out.print("Digite o numero de portas: ");
        int numeroPortas = scanner.nextInt();

        System.out.print("Digite a cor do carro: ");
        String cor = scanner.next();

        System.out.print("Digite o numero do chassi: ");
        String chassi = scanner.next();

        System.out.print("Digite o ano de fabricação: ");
        int anoFabricacao = scanner.nextInt();
        
        System.out.print("Digite o quanto o carro usa de combustível: ");
        float combustivel = scanner.nextFloat();

        Carro novoCarro = new Carro(numeroPortas, chassi, anoFabricacao, combustivel);
        novoCarro.getAnoFabricacao();
        novoCarro.getChassi();
        novoCarro.setCor(cor);
        novoCarro.getNumeroPortas();
        novoCarro.getCombustivel();

        System.out.println("O numero de portas é " + novoCarro.getNumeroPortas());
        System.out.println("A cor do seu carro é " + novoCarro.getCor());
        System.out.println("O seu numero de Chassi é " + novoCarro.getChassi());
        System.out.println("O ano do seu carro é " + novoCarro.getAnoFabricacao());
        System.out.println("O seu carro usa " + novoCarro.getCombustivel() + "L de combustível");
    }
}
