package models;
    
public class Carro {
    int numeroPortas;
    String cor;
    String chassi;
    int anoFabricacao;
    float combustivel;

    public Carro(int numeroPortas, String chassi, int anoFabricacao, float combustivel){
        this.numeroPortas = numeroPortas;
        this.chassi = chassi;
        this.anoFabricacao = anoFabricacao;
        this.combustivel = combustivel;
    }
    public void setCor(String cor){
        this.cor = cor;
    }
    public int getNumeroPortas(){
        return numeroPortas;
    }
    public String getCor(){
        return cor;
    }
    public String getChassi(){
        return chassi;
    }
    public int getAnoFabricacao(){
        return anoFabricacao;
    }
    public float getCombustivel(){
        return combustivel;
    }
}